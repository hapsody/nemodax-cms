var express = require('express');
var url  = require('url');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var mysql = require('mysql');
var utils = require('../../lib/utils');
let jwt = require("jsonwebtoken");
const mybatis = require('../../lib/mybatis');

var router = express.Router();


/* login porcess */
router.get('/login', function(req, res, next) {
  console.log('/api/admin/login');
  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    user_id : params.user_id,
    password : params.pwd
  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('member', 'selectCmsUser', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(results);
    let cms_user = results[0];

    console.log(cms_user);
    if(cms_user !== undefined){
      //token 을 만든다.
      let token = jwt.sign({
        user_id: cms_user.user_id   // 토큰의 내용(payload)
      },
      'nemodax_cms' ,    // 비밀 키
      {
        expiresIn: '300m'    // 유효 시간은 300분
      })

      //쿠키를 생성한다.
      let options = {
        httpOnly: true, // The cookie only accessible by the web server
        path: '/',
        maxAge: 3000 * 60 * 60 // 쿠키 유효기간 3시간
      }
      res.cookie('NMCT', token, options);
      res.json(cms_user);

    }else{
      let err_json = `{"err_code" : 1 , "err_msg" : "아이디 또는 비밀번호가 일치 하지 않습니다."}`;
      res.json(JSON.parse(err_json));
    }
  });

  //
  // cms_user.findOne(
  //   {
  //     where : {
  //       user_id : params.user_id,
  //       password : params.pwd
  //     }
  //   }
  // ).then(result =>{
  //   let cms_user = utils.queryResultParse(result);
  //   console.log(cms_user);
  //   if(cms_user !== null){
  //     //token 을 만든다.
  //     let token = jwt.sign({
  //       email: "foo@example.com"   // 토큰의 내용(payload)
  //     },
  //     'nemodax_cms' ,    // 비밀 키
  //     {
  //       expiresIn: '300m'    // 유효 시간은 300분
  //     })
  //
  //     //쿠키를 생성한다.
  //     let options = {
  //       httpOnly: true, // The cookie only accessible by the web server
  //       path: '/',
  //       maxAge: 3000 * 60 * 60 // 쿠키 유효기간 3시간
  //     }
  //     res.cookie('NMCT', token, options);
  //     res.json(cms_user);
  //
  //   }else{
  //     let err_json = `{"err_code" : 1 , "err_msg" : "아이디 또는 비밀번호가 일치 하지 않습니다."}`;
  //     res.json(JSON.parse(err_json));
  //   }
  // }).catch(err => {
  //   console.error('Unable to connect to the database:', err);
  //   let err_json = `{"err_code" : 9999 , "err_msg" : "System Error"}`;
  //   res.json(JSON.parse(err_json));
  // });


});



module.exports = router;

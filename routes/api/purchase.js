var express = require('express');
var url  = require('url');
var path = require('path');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var utils = require('../../lib/utils');
let jwt = require("jsonwebtoken");
const mybatis = require('../../lib/mybatis');


var router = express.Router();



/* list porcess */
router.get('/count', function(req, res, next) {
  console.log('/api/purchase/count');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {interval_value: params.graph_size}

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('purchase', 'selectPurchaseCntPerDay', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    console.log("db_purchase_list: ", results)
    dataForChart = {
      db_data: results,
      db_length: results.length,
      graph_data: new Array(),
      graph_size: params.graph_size,
      i:0,
      j:0,
      endOfdata:false,
      startDay:utils.getFormattedDay(new Date())
    }
    resultData = utils.addZeroCntToGraph(dataForChart);

    result.data = resultData;

    res.json(result);

  });

});



module.exports = router;

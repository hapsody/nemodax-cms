var express = require('express');
var url = require('url');
var path = require('path');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var utils = require('../../lib/utils');
const mybatis = require('../../lib/mybatis');
const Web3 = require('web3');
const ethers = require('ethers');

var router = express.Router();


/* list porcess */
router.get('/contentList', function(req, res, next) {
  console.log('/api/content/contentList');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    title: params.title,
    start: params.start,
    length: params.length

  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectContentList', param, format);
  console.log('query :: ', query);
  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('content', 'selectContentListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      //console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);

      res.json(result);
    });
  });

});


/* content detail */
router.get('/contentDetail', function(req, res, next) {
  console.log('/api/content/contentDetail');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    content_no: params.content_no
  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectContentDetail', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results[0];

    res.json(result);
  });
});


/* list porcess */
router.get('/fileList', function(req, res, next) {
  console.log('/api/content/fileList');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    content_no: params.content_no
  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectFileList', param, format);
  //console.log('query :: ', query);
  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results;
    res.json(result);
  });

});


/* file detail */
router.get('/fileDetail', function(req, res, next) {
  console.log('/api/content/fileDetail');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    sale_file_no: params.sale_file_no
  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectFileDetail', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results[0];

    res.json(result);
  });
});


/* purchase list porcess */
router.get('/purchaseList', function(req, res, next) {
  console.log('/api/content/purchaseList');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    purchase_stat: params.purchase_stat,
    mem_id: params.mem_id,
    start: params.start,
    length: params.length
  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectPurchaseList', param, format);
  console.log('query :: ', query);
  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('content', 'selectPurchaseListCnt', param, format);
    console.log('query :: ', query);
    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);

      res.json(result);
    });
  });
});


/* purchase detail */
router.get('/purchaseDetail', function(req, res, next) {
  console.log('/api/content/purchaseDetail');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    purchase_no: params.purchase_no
  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectPurchaseDetail', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results[0];

    res.json(result);
  });
});


/* 환불 처리 프로세스 */
router.post('/refundProcess', wrapAsync(async function(req, res, next) {
  console.log('/api/content/reportResultProcess');

  var params = req.body;
  console.log("params: ",params);
  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectPurchaseDetail', {'purchase_no': params.purchase_no}, format);

  //Query
  let results = await mybatis.connection.promise().query(query);
  let purchase = (results[0])[0];
  //console.log('purchase :: ', purchase);

  await mybatis.connection.promise().beginTransaction().catch(err => {
    console.log('start transaction err :: ');
    throw err;
  });

  console.log('start transaction :: ');

  try{
    //환불 처리 결과가 01(환불처리완료) 일 경우만 포인트 관련 처리
    if(params.refund_result === '01'){

      /*
        구매자 포인트 지급.
      */
      //구매자 포인트 조회
      query = mybatis.mybatisMapper.getStatement('member', 'selectPoint', {mem_no: purchase.mem_no}, format);
      results = await mybatis.connection.promise().query(query);
      let point = (results[0])[0];
      console.log('point :: ' , point[0]);

      //구매자 포인트 지급
      query = mybatis.mybatisMapper.getStatement('member', 'updatePoint',
                    {
                      mem_no: purchase.mem_no,
                      point : point.point + purchase.point
                    }, format);
      let update = await mybatis.connection.promise().query(query)
        .catch(async err => {
          await mybatis.connection.promise().rollback();
          throw err;
        });
      console.log('update :: ' , update);

      //구매자 포인트 히스토리 등록
      query = mybatis.mybatisMapper.getStatement('member', 'insertPointHistory',
                    {
                      mem_no: purchase.mem_no,
                      use_type: 'PURCHASE_REFUND',
                      point : purchase.point,
                      memo: null,
                      purchase_group_no: purchase.purchase_group_no,
                      purchase_no: purchase.purchase_no
                    }, format);
      let insert = await mybatis.connection.promise().query(query)
        .catch(async err => {
          await mybatis.connection.promise().rollback();
          throw err;
        });
      console.log('insert :: ' , insert);


      /*
        판매자 포인트 지급.
      */
      //판매자 포인트 조회
      query = mybatis.mybatisMapper.getStatement('member', 'selectPoint', {mem_no: purchase.sale_mem_no}, format);
      results = await mybatis.connection.promise().query(query);
      let sale_mem_point = (results[0])[0];
      console.log('sale member point :: ' , point[0]);

      //판매자 포인트 차감
      query = mybatis.mybatisMapper.getStatement('member', 'updatePoint',
                    {
                      mem_no: purchase.sale_mem_no,
                      point : sale_mem_point.point - purchase.point
                    }, format);
      update = await mybatis.connection.promise().query(query)
        .catch(async err => {
          await mybatis.connection.promise().rollback();
          throw err;
        });
      console.log('update :: ' , update);


      //판매자 포인트 히스토리 등록
      query = mybatis.mybatisMapper.getStatement('member', 'insertPointHistory',
                    {
                      mem_no: purchase.sale_mem_no,
                      use_type: 'SALE_REFUND',
                      point : purchase.point,
                      memo: null,
                      purchase_group_no: purchase.purchase_group_no,
                      purchase_no: purchase.purchase_no
                    }, format);
      insert = await mybatis.connection.promise().query(query)
        .catch(async err => {
          await mybatis.connection.promise().rollback();
          throw err;
        });
      console.log('insert :: ' , insert);

    }

    //purchase 상태 변경
    query = mybatis.mybatisMapper.getStatement('content', 'updatePurchase',
                  {
                    purchase_no: params.purchase_no,
                    stat: params.stat,
                    refund_result: params.refund_result
                  }, format);

    let stat_update = await mybatis.connection.promise().query(query)
      .catch(async err => {
        await mybatis.connection.promise().rollback();
        throw err;
      });
    console.log('stat update :: ' , stat_update);
  }catch(err){
    await mybatis.connection.promise().rollback();

    throw err;
  }





  //commit
  try{
    await mybatis.connection.promise().commit();
    console.log('commit success!');
  }catch(err){
    connection.rollback(function() {
      throw err;
    });
  }



  // mybatis.connection.promise().beginTransaction(async function(err) {
  //   if (err) {
  //     console.log('start transaction err :: ');
  //     throw err;
  //   }
  //
  //
  //
  //
  // });



  console.log('..end');
  res.json('');

}));


/* report list porcess */
router.get('/reportList', function(req, res, next) {
  console.log('/api/content/reportList');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    start: params.start,
    length: params.length

  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectReportList', param, format);
  console.log('query :: ', query);
  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('content', 'selectReportListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);

      res.json(result);
    });
  });
});


/* report detail */
router.get('/reportDetail', function(req, res, next) {
  console.log('/api/content/reportDetail');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
    file_report_no: params.file_report_no
  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'selectReportDetail', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results[0];

    res.json(result);
  });
});


/* 신고 결과 처리 프로세스 */
router.post('/reportResultProcess', function(req, res, next) {
  console.log('/api/content/reportResultProcess');

  var params = req.body;
  console.log("params: ",params);

  //코인 나가는 부분이니 로그인 아이디 비밀번호 체크
  // SQL Parameters
  var param = {
    user_id: params.user_id,
    password: params.pwd
  }

  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('member', 'selectCmsUser', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(results);
    let cms_user = results[0];

    if (cms_user !== undefined) {

      let tx_id = null;
      if (params.reward_type === '01') {
        //회원의 일반 지갑 주소 조회
        var param2 = {
          mem_no: params.mem_no,
          wallet_type: 'REGULAR'
        }
        // Get SQL Statement
        var format = {
          language: 'sql',
          indent: '  '
        };
        var query = mybatis.mybatisMapper.getStatement('member', 'selectWalletList', param2, format);



        //Query
        mybatis.connection.query(query, function(err, results, fields) {
          console.log(results);
          let receiver_wallet = results[0];
          if (receiver_wallet !== undefined) {
            //지급 완료이면 2코인 지급
            console.log('Receiver wallet address', receiver_wallet.wallet_address);

            // keystore 파일과 비밀번호 가져오기
            let json = JSON.stringify(req.config.info.data);
            let password = "test0";

            ethers.Wallet.fromEncryptedJson(json, password).then(async function(wallet) {
              console.log("Sender Address: " + wallet.address);

              var provider = new ethers.providers.InfuraProvider(network = req.config.info.blockchain_network, apiAccessToken = "ae10d752a79f45ea84f5d1e32cb621aa")
              var walletWithProvider = new ethers.Wallet(wallet.privateKey, provider);

              walletWithProvider.getTransactionCount().then(function(nonce) {

                console.log("nonce: ", nonce);
                targetAddress = utils.pad(receiver_wallet.wallet_address.slice(2), 64);

                var amountHex = utils.pad(parseInt(2 * 1000000000000000000).toString(16), 64);
                var data = utils.getFuncHexData("transfer(address,uint256)", targetAddress, amountHex);

                var tx = {
                  to:req.config.info.erc20_sc_address,
                  data: data,
                  nonce: nonce,
                };
                var startTime;
                walletWithProvider.sendTransaction(tx).then(function(tx){
                  startTime = new Date();
                  console.log("tx_id: ", tx.hash);
                  tx_id = tx.hash;
                  console.log("provider: ", provider.connection);
                  provider.waitForTransaction(tx.hash).then((receipt) => {
                    console.log('Transaction Mined: ' + receipt.transactionHash);
                    console.log('confirmations: ' + receipt.confirmations);
                    console.log("Used Gas: " + parseInt(receipt.gasUsed._hex));
                    var endTime = new Date();
                    var intervalTime = endTime - startTime;
                    console.log("intervalTime: " + intervalTime + "ms");

                    //db 업데이트
                    updateReportResult(params.file_report_no, params.result_type, params.reward_type, tx_id);
                    res.json('');
                    // result = {data:receipt}
                    // res.json(result)
                  });
                });

              });
            });
          }
        });
      }else{
        //db 업데이트
        updateReportResult(params.file_report_no, params.result_type, params.reward_type, tx_id);
        res.json('');
      }


    } else {
      let err_json = `{"err_code" : 1 , "err_msg" : "아이디 또는 비밀번호가 일치 하지 않습니다."}`;
      res.json(JSON.parse(err_json));
    }
  });

});

/*
  신고 테이블 업데이트
*/
async function updateReportResult(file_report_no, result_type, reward_type, reward_tx_id){
  var format = {
    language: 'sql',
    indent: '  '
  };
  var query = mybatis.mybatisMapper.getStatement('content', 'updateReport',
                {
                  file_report_no: file_report_no,
                  result_type: result_type,
                  reward_type: reward_type,
                  reward_tx_id: reward_tx_id
                }, format);
  let stat_update = await mybatis.connection.promise().query(query);
  console.log('report update :: ' , stat_update);
}


/*
  async 에러 처리를 위해...
*/
function wrapAsync(fn) {
  return function(req, res, next) {
    // 모든 오류를 .catch() 처리하고 체인의 next() 미들웨어에 전달하세요
    // (이 경우에는 오류 처리기)
    fn(req, res, next).catch(next);
  };
}


module.exports = router;

var express = require('express');
var url  = require('url');
var path = require('path');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var utils = require('../../lib/utils');
let jwt = require("jsonwebtoken");
const mybatis = require('../../lib/mybatis');

var router = express.Router();


router.get('/delete', function(req,res,next){
  console.log('/api/notice/delete');
  let result ={};
  var params = url.parse(req.url,true).query;
  var param = {
    notice_no:params.notice_no,
  }
  console.log("Received Params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('notice', 'deleteNoticeList', param, format);
  mybatis.connection.query(query, function(err,results,fields){
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});


router.get('/modify', function(req,res,next){
  console.log('/api/notice/modify');
  let result ={};
  var params = url.parse(req.url,true).query;
  var param = {
    notice_no:params.notice_no,
    title:params.title,
    message:params.message,

  }
  console.log("Received Params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('notice', 'modifyNoticeList', param, format);
  mybatis.connection.query(query, function(err,results,fields){
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});

router.get('/create', function(req,res,next){
  console.log('/api/notice/create');
  let result ={};
//  result.data = "done";

  var params = url.parse(req.url, true).query;
  var param = {
    title : params.title,
    message : params.message,
  }
  console.log("Received params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('notice', 'createNoticeList', param, format)

  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results
    if(err){
      result.err = err;
    }

    res.json(result);

  });


})



/* selectItem process */
router.get('/selectItem', function(req, res, next) {
  console.log('/api/notice/selectItem');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      notice_no : params.notice_no,

  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('notice', 'selectNoticeItem', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });

});


/* list porcess */
router.get('/list', function(req, res, next) {
  console.log('/api/notice/list');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      title : params.title,
      message : params.message,
      start : params.start,
      length : params.length

  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('notice', 'selectNoticeList', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('notice', 'selectNoticeListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      //console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);
      console.log("results[0]: ", results[0]);
      res.json(result);

    });
  });

});



module.exports = router;

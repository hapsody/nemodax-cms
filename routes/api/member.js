var express = require('express');
var url  = require('url');
var path = require('path');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var utils = require('../../lib/utils');
let jwt = require("jsonwebtoken");
const mybatis = require('../../lib/mybatis');
let moment = require('moment');

var router = express.Router();


/* list porcess */
router.get('/list', function(req, res, next) {
  console.log('/api/member/list');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      name : params.name,
      mem_id : params.mem_id,
      start : params.start,
      length : params.length

  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('member', 'selectMemberList', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('member', 'selectMemberListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      //console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);

      res.json(result);
    });
  });

});



/* member detail */
router.get('/detail', function(req, res, next) {
  console.log('/api/member/detail');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      mem_no : params.mem_no
  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('member', 'selectMemberDetail', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results[0];

    res.json(result);
  });
});

/* wallet list */
router.get('/walletList', function(req, res, next) {
  console.log('/api/member/walletList');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      mem_no : params.mem_no
  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('member', 'selectWalletList', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    //console.log(results);
    result.data = results;

    res.json(result);
  });
});



/* 일별 가입 수 조회 */
router.get('/selectMemberCountByDay', function(req, res, next) {
  console.log('/api/member/selectMemberCountByDay');
  let result = {};

  var params = url.parse(req.url, true).query;

  let interval_value = params.graph_size;
  // SQL Parameters
  var param = {interval_value: interval_value}

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('member', 'selectMemberCountByDay', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {

    let startDay = moment().add(interval_value * -1, "days").format("YYYYMMDD");
    let endDay = moment().format("YYYYMMDD");

    let day = startDay;

    //key : 날짜 , value : cnt 변형 - 조회된 날짜가 있는지 없는지 판단하기 쉽게 하기 위함.
    let tempObject = {};  //{ '20190524': 2, '20190530': 2, '20190617': 1 }
    for(let idx in results){
      tempObject[results[idx].join_date] = results[idx].cnt;
    }

    let result = [];
    while(true){
      let obj = {};
      obj.join_date = day;
      if(tempObject.hasOwnProperty(day)){
        obj.cnt = tempObject[day];
      }else{
        obj.cnt = 0;
      }

      result.push(obj);

      if(day === endDay){
        break;
      }

      day = moment(day).add(1, 'days').format("YYYYMMDD");
    }

    console.log('result :: ', result);
    res.json(result);

  });

});


/* member detail */
router.get('/pointHistory', function(req, res, next) {
  console.log('/api/member/pointHistory');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      mem_no : params.mem_no
  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('member', 'selectPointHistoryList', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('member', 'selectPointHistoryListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      //console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);
      console.log("results[0]: ", results[0]);
      res.json(result);

    });

  });
});




/* 포인트 지급 */
router.post('/payPoint', wrapAsync(async function(req, res, next) {
  console.log('/api/member/payPoint');
  for (var i in req.body){
    console.log(i);
  }

  var params = req.body;

  console.log("params: ",params);
  // Get SQL Statement
  var format = {
    language: 'sql',
    indent: '  '
  };


  await mybatis.connection.promise().beginTransaction().catch(err => {
    console.log('start transaction err :: ');
    throw err;
  });

  console.log('start transaction :: ');

  try{

      /*
        포인트 지급.
      */
      //회원 포인트 조회
      query = mybatis.mybatisMapper.getStatement('member', 'selectPoint', {mem_no: params.mem_no}, format);
      results = await mybatis.connection.promise().query(query);
      let point = (results[0])[0];
      console.log('point :: ' , point[0]);

      //포인트 지급
      query = mybatis.mybatisMapper.getStatement('member', 'updatePoint',
                    {
                      mem_no: params.mem_no,
                      point : point.point + Number(params.point)
                    }, format);
      let update = await mybatis.connection.promise().query(query)
        .catch(async err => {
          await mybatis.connection.promise().rollback();
          throw err;
        });
      console.log('update :: ' , update);

      //포인트 히스토리 등록
      query = mybatis.mybatisMapper.getStatement('member', 'insertPointHistory',
                    {
                      mem_no: params.mem_no,
                      use_type: 'ADMIN',
                      point : params.point,
                      memo: params.memo,
                      purchase_group_no: null,
                      purchase_no: null
                    }, format);
      let insert = await mybatis.connection.promise().query(query)
        .catch(async err => {
          await mybatis.connection.promise().rollback();
          throw err;
        });
      console.log('insert :: ' , insert);



  }catch(err){
    await mybatis.connection.promise().rollback();

    throw err;
  }

  //commit
  try{
    await mybatis.connection.promise().commit();
    console.log('commit success!');
  }catch(err){
    connection.rollback(function() {
      throw err;
    });
  }



  console.log('..end');
  res.json('');

}));

/*
  async 에러 처리를 위해...
*/
function wrapAsync(fn) {
  return function(req, res, next) {
    // 모든 오류를 .catch() 처리하고 체인의 next() 미들웨어에 전달하세요
    // (이 경우에는 오류 처리기)
    fn(req, res, next).catch(next);
  };
}

module.exports = router;

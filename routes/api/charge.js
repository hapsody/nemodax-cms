var express = require('express');
var url  = require('url');
var path = require('path');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var utils = require('../../lib/utils');
let jwt = require("jsonwebtoken");
const mybatis = require('../../lib/mybatis');

var router = express.Router();


router.get('/detail', function(req,res,next){
  console.log('/api/charge/detail');
  let result ={};
  var params = url.parse(req.url,true).query;
  var param = {
    charge_no:params.charge_no,
  }
  console.log("Received Params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('charge', 'selectChargeDetail', param, format);
  mybatis.connection.query(query, function(err,results,fields){
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});



/* list porcess */
router.get('/list', function(req, res, next) {
  console.log('/api/charge/list');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      name : params.name,
      wallet_address : params.wallet_address,
      transaction_id: params.transaction_id,
      start : params.start,
      length : params.length

  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('charge', 'selectChargeList', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('charge', 'selectChargeListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      //console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);
      console.log("results[0]: ", results[0]);
      res.json(result);

    });
  });

});



module.exports = router;

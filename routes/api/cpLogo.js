var express = require('express');
var url  = require('url');
var path = require('path');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var utils = require('../../lib/utils');
let jwt = require("jsonwebtoken");
const mybatis = require('../../lib/mybatis');

var router = express.Router();

// Set AWS configuration for SQS
var AWS = require('aws-sdk');

// ~/.aws/credentials
var credentials = new AWS.SharedIniFileCredentials({profile: 'dev_access_key'});

// Set the region
AWS.config.update({
  region: 'ap-northeast-2',
  credentials:credentials
});

// Create an SQS service object
var sqs = new AWS.SQS({
  apiVersion: '2012-11-05'
});






router.get('/delete', function(req,res,next){
  console.log('/api/cpLogo/delete');
  let result ={};
  var params = url.parse(req.url,true).query;
  var param = {
    cplogo_no:params.cplogo_no,
  }
  console.log("Received Params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('cpLogo', 'deleteCPLOGOList', param, format);
  mybatis.connection.query(query, function(err,results,fields){
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});


router.get('/modify', function(req,res,next){
  console.log('/api/cpLogo/modify');
  let result ={};
  var params = url.parse(req.url,true).query;
  var param = {
    cplogo_no:params.cplogo_no,
    title:params.title,
    desc:params.desc,
    link:params.link,
    img_path:params.img_path,
  }
  console.log("Received Params: ", params);

  // SQS Queueing
  var sqs_params = {
    DelaySeconds: 10,
    MessageBody: "CPLOGO",
    QueueUrl: req.config.info.sqs_url
  };
  sqs.sendMessage(sqs_params, function(err, data) {
    if (err) {
      console.log("Error", err);
    } else {
      console.log("Success", data.MessageId);
    }
  });


  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('cpLogo', 'modifyCPLOGOList', param, format);
  mybatis.connection.query(query, function(err,results,fields){
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});

router.get('/create', function(req,res,next){
  console.log('/api/cpLogo/create');
  let result ={};
//  result.data = "done";

  var params = url.parse(req.url, true).query;
  var param = {
    title : params.title,
    desc : params.desc,
    link : params.link,
    img_path:params.img_path,
  }
  console.log("Received params: ", params);

  var sqs_params = {
    DelaySeconds: 10,
    MessageBody: "CPLOGO",
    QueueUrl: req.config.info.sqs_url
  };
  sqs.sendMessage(sqs_params, function(err, data) {
    if (err) {
      console.log("Error", err);
    } else {
      console.log("Success", data.MessageId);
    }
  });


  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('cpLogo', 'createCPLOGOList', param, format)

  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results
    if(err){
      result.err = err;
    }

    res.json(result);

  });


})



/* selectItem process */
router.get('/selectItem', function(req, res, next) {
  console.log('/api/cpLogo/selectItem');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      cplogo_no : params.cplogo_no,

  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('cpLogo', 'selectCPLOGOItem', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });

});


/* list porcess */
router.get('/list', function(req, res, next) {
  console.log('/api/cpLogo/list');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      title : params.title,
      desc : params.desc,
      start : params.start,
      length : params.length
  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('cpLogo', 'selectCPLOGOList', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);

    for(var r in results){
      results[r].img_url = `${req.config.info.file_server_url}/${results[r].img_path}`;
    }

    result.data = results;

    query = mybatis.mybatisMapper.getStatement('cpLogo', 'selectCPLOGOListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      //console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);
      console.log("results[0]: ", results[0]);
      res.json(result);

    });
  });

});



module.exports = router;

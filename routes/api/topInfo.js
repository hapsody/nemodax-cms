var express = require('express');
var url  = require('url');
var path = require('path');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var utils = require('../../lib/utils');
let jwt = require("jsonwebtoken");
const mybatis = require('../../lib/mybatis');

var router = express.Router();


router.get('/delete', function(req,res,next){
  console.log('/api/topInfo/delete');
  let result ={};
  var params = url.parse(req.url,true).query;
  var param = {
    top_notice_no:params.top_notice_no,
  }
  console.log("Received Params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('topNotice', 'deleteTopNoticeList', param, format);
  mybatis.connection.query(query, function(err,results,fields){
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});


router.get('/modify', function(req,res,next){
  console.log('/api/topInfo/modify');
  let result ={};
  var params = url.parse(req.url,true).query;
  var param = {
    top_notice_no:params.top_notice_no,
    date:params.date,
    title:params.title,
    desc:params.desc,
    link:params.link,
  }
  console.log("Received Params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('topNotice', 'modifyTopNoticeList', param, format);
  mybatis.connection.query(query, function(err,results,fields){
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});

router.get('/create', function(req,res,next){
  console.log('/api/topInfo/create');
  let result ={};
//  result.data = "done";

  var params = url.parse(req.url, true).query;
  var param = {
    date: params.date,
    title : params.title,
    desc : params.desc,
    link : params.link,
  }
  console.log("Received params: ", params);

  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('topNotice', 'createTopNoticeList', param, format)

  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results
    if(err){
      result.err = err;
    }
    res.json(result);
  });
});


/* selectItem process */
router.get('/selectItem', function(req, res, next) {
  console.log('/api/topInfo/selectItem');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      top_notice_no : params.top_notice_no,
  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('topNotice', 'selectTopNoticeItem', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results;
    if(err){
      result.err = err;
    }
    res.json(result);
  });

});




/* list porcess */
router.get('/list', function(req, res, next) {
  console.log('/api/topInfo/list');
  let result = {};
  result.draw = res.draw;

  var params = url.parse(req.url, true).query;

  // SQL Parameters
  var param = {
      title : params.title,
      desc : params.desc,
      start : params.start,
      length : params.length

  }

  // Get SQL Statement
  var format = {language: 'sql', indent: '  '};
  var query = mybatis.mybatisMapper.getStatement('topNotice', 'selectTopNoticeList', param, format);

  //Query
  mybatis.connection.query(query, function(err, results, fields) {
    console.log(query);
    result.data = results;

    query = mybatis.mybatisMapper.getStatement('topNotice', 'selectTopNoticeListCnt', param, format);

    mybatis.connection.query(query, function(err, results, fields) {
      //console.log(results);
      //console.log(results[0].cnt);
      result.recordsFiltered = results[0].cnt;
      //console.log(fields);
      console.log("results[0]: ", results[0]);
      res.json(result);

    });
  });

});



module.exports = router;

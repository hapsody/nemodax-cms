var express = require('express');
var http = require('http');
var qs = require('querystring');
var url  = require('url');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('index', { title: 'NemoDax' });
});



/*  */
router.get('/login', function(req, res, next) {
  res.render('login', { title: 'NemoDax' , layout: 'layouts/layout-no'});
});


router.get('/view/*/*', function(req,res, next){
  let first_path = req.path.split('/')[2];
  let last_path = req.path.split('/')[3];

  let obj = new Object();
  obj.title = 'Nemodax';

  if(last_path == 'pointHistory'){
    obj.layout = 'layouts/layout-no';
  }

  Object.assign(obj, req.query);

  res.render(`${first_path}/${last_path}`, obj);

});

module.exports = router;

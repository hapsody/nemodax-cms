function inputChartData(data) {

    Morris.Bar({
        element: 'morris-bar-chart',
        data: data,
        xkey: 'date',
        ykeys: ['buy'],
        labels: ['Series A'],
        hideHover: 'auto',
        resize: true
    });

};

let rate;

$(function () {
  chargeMain();
});

/*
  충전 비율을 가져온다.
*/
function getChargeRate() {
  var dataMap = {
    'call_url': '/api/charge/rate.do'
  }

  $.ajax({
    type: "GET",
    url: "/api/getApi",
    data: dataMap,
    dataType: "json",
    success: function(response) {
      if (response.result.code === 0) {
        rate = response.data;
      }else if(response.result.code === 999010 || response.result.code === 999011){
        location.href = '/member/login';
      }
    },
    error: function(data) {
      console.log('error');
      console.log(data);
    }
  });
}


/*
  키스토어파일 로드
*/
let keyStoreJson_eth;
let keyStoreJson_coin;
function loadFromKeyStoreFileEth(event){
  var keystoreFile = event.target.files[0];

  if (!keystoreFile) {
    return;
  }
  var fileReader = new FileReader();
  fileReader.readAsText(keystoreFile);

  fileReader.onloadend = async function(e) {
    keyStoreJson_eth = e.target.result;
  };

  $('#file_name_eth').val(keystoreFile.name);

}
function loadFromKeyStoreFileCoin(event){
  var keystoreFile = event.target.files[0];

  if (!keystoreFile) {
    return;
  }
  var fileReader = new FileReader();
  fileReader.readAsText(keystoreFile);

  fileReader.onloadend = async function(e) {
    keyStoreJson_coin = e.target.result;
  };

  $('#file_name_coin').val(keystoreFile.name);
}

/*
  충전 메인
*/
function chargeMain(){
  let dataMap = {
  };
  let html = $('#charge_main')[0].innerHTML;
  $('#popup-charge01').empty();
  $.tmpl(html, dataMap).appendTo('#popup-charge01');

}



/*
  코인으로 포인트 충전
*/
function coinPoint1(){
  getChargeRate();

  let dataMap = {
  };
  let html = $('#charge_coin_point1')[0].innerHTML;
  $('#popup-charge01').empty();
  $.tmpl(html, dataMap).appendTo('#popup-charge01');

  //cash,eth로 coin 충전 계산
  $("#charge_point").keyup(function() {
    let coin_point = rate.COIN_POINT;
    let coin = $("#charge_point").val() / coin_point;
    $("#charge_coin").val(addComma(coin + ''));
  });
  $('#charge_point').keypress(function (event) {
    if (event.which && (event.which <= 47 || event.which >= 58) && event.which != 8) {
      event.preventDefault();
    }
  });
}

function coinPoint2(){
  //
  if($("#charge_point").val() < 1){
    showCommonPopup('안내', '1이상을 입력해주세요.', 'OK', function (){
    });
    return;
  }

  //일반 지갑 존재 여부 체크
  if(isEmpty(wallet_regular)){
    showCommonPopup('안내', '일반 지갑을 먼저 생성해주세요.', 'OK', function (){
    });
    return;
  }

  let point = $("#charge_point").val();
  let coin = removeComma($("#charge_coin").val());

  if($.trim(point) === '' || point <= 0){
    showCommonPopup('안내', '수량을 입력해주세요.', 'OK', function (){
      $("#charge_point").focus();
    });
    return;
  }

  if(wallet_regular.coin_balance < coin){
    showCommonPopup('안내', '보유 NEMOCoin이 부족합니다.', 'OK', function (){

    });
    return;
  }

  let dataMap = {
    'point' : addComma(point),
    'coin' : addComma(coin)
  };
  let html = $('#charge_coin_point2')[0].innerHTML;
  $('#popup-charge01').empty();
  $.tmpl(html, dataMap).appendTo('#popup-charge01');

  $('#popup-charge01 .btn_bottom').click(function (){
    coinPoint3(point, coin);
  });

}

async function coinPoint3(point, coin){

  await tokenCheck();

  //keystorefile 체크
  if(isEmpty(keyStoreJson_coin)){
    showCommonPopup('안내', 'KEY STORE FILE을 선택해주세요.', 'OK', function (){
    });
    return;
  }

  //레귤러 주소랑 keystorefile랑 같은지 체크
  if(wallet_regular.wallet_address !== ('0x' + JSON.parse(keyStoreJson_coin).address)){
    showCommonPopup('안내', '등록된 지갑과 다른 KEY STORE FILE 입니다.', 'OK', function (){
    });
    return;
  }

  //비밀번호 입력 체크
  let charge_transaction_pwd = $('#charge_transaction_pwd').val();
  if($.trim(charge_transaction_pwd) === ''){
    showCommonPopup('안내', '비밀번호를 입력해주세요.', 'OK', function (){
      $('#charge_transaction_pwd').focus();
    });
    return;
  }


  //트랜잭션 호출
  let progressObj = $('#popup-charge01 .loading');
  $('#popup-charge01 #transaction_btn').hide();

  let tx = await tokenTransfer(point_charge_address, charge_transaction_pwd, coin, keyStoreJson_coin, progressObj)
    .catch(function (e){
      //progressObj.hide();
      progressObj.addClass('hidden');
      $('#popup-charge01 #transaction_btn').show();
      showCommonPopup('안내', e.message, 'OK', function (){
      });
    }
  );

  if(!isEmpty(tx)){
    let dataMap = {
      'call_url' : '/api/charge/coinPoint.do',
      'wallet_no' : wallet_regular.wallet_no,
      'purchase_amount' : point,
      'deposit_price' : coin,
      'transaction_id' : tx.hash
    };

    $.ajax({
      type: "POST",
      url: "/api/postApi",
      data : dataMap,
      dataType: "json",
      success: function( data ) {
        if(data.result.code === 0){
          let dMap = {
            'point' : point,
            'coin' : coin,
            'transaction_id' : tx.hash
          };
          let html = $('#charge_coin_point3')[0].innerHTML;
          $('#popup-charge01').empty();
          $.tmpl(html, dMap).appendTo('#popup-charge01');
        }else if(data.result.code === 999010 || data.result.code === 999011){
          location.href = '/member/login';
        }else{
          alert(`code : ${data.result.code} , msg : ${data.result.msg}`);
        }
      },
      error: function( data ) {
        console.log('error');
        console.log(data);
      }
    });
  }
}


/*
  코인 충전 선택화면
*/
function chargeConinSelect(){
  getChargeRate();

  let dataMap = {
  };
  let html = $('#charge_coin_select')[0].innerHTML;
  $('#popup-charge01').empty();
  $.tmpl(html, dataMap).appendTo('#popup-charge01');

  //cash,eth로 coin 충전 계산
  $("#charge_coin").keyup(function() {
    let cash_coin = rate.CASH_COIN;
    let cash = $("#charge_coin").val() / cash_coin;
    $("#charge_cash").val(addComma(cash + ''));

    let eth_coin = rate.ETH_COIN;
    let coin = $("#charge_coin").val() / eth_coin;
    $("#charge_eth").val(addComma(coin + ''));
  });

  $('#charge_coin').keypress(function (event) {
    if (event.which && (event.which <= 47 || event.which >= 58) && event.which != 8 && event.which != 46) {
      event.preventDefault();
    }
  });
}

/*
  캐시로 코인 충전
*/
function cashCoin1(){
  if($("#charge_coin").val() < 0.0001){
    showCommonPopup('0.0001이상을 입력해주세요.', '', 'OK', function (){
    });
    return;
  }

  if(isEmpty(wallet_regular)){
    showCommonPopup('일반 지갑을 먼저 생성해주세요.', '', 'OK', function (){
    });
    return;
  }

  let coin = $("#charge_coin").val();
  let cash = removeComma($("#charge_cash").val());
  if($.trim(coin) === '' || coin <= 0){
    showCommonPopup('수량을 입력해주세요.', '', 'OK', function (){
      $("#charge_coin").focus();
    });
    return;
  }


  let dataMap = {
    'coin' : addComma(coin),
    'cash' : addComma(cash)
  };
  let html = $('#charge_cash_coin1')[0].innerHTML;
  $('#popup-charge01').empty();
  $.tmpl(html, dataMap).appendTo('#popup-charge01');
}

function cashCoin2(){
  var purchase_amount = removeComma($('#charge_coin').val());
  var deposit_price = removeComma($('#charge_cash').val());
  let depositor = $('#depositor').val();

  if($.trim(depositor) === ''){
    showCommonPopup('입금자명을 입력해주세요.', '', 'OK', function (){
      $('#depositor').focus();
    });
    return;
  }

  let dataMap = {
    'call_url' : '/api/charge/cashCoin.do',
    'wallet_no' : wallet_regular.wallet_no,
    'purchase_amount' : purchase_amount,
    'deposit_price' : deposit_price,
    'depositor' : depositor
  };

  $.ajax({
    type: "POST",
    url: "/api/postApi",
    data : dataMap,
    dataType: "json",
    success: function( data ) {
      //console.log(data);
      if(data.result.code === 0){
        let dMap = {
          'coin' : $('#charge_coin').val(),
          'cash' : $('#charge_cash').val()
        }
        let html = $('#charge_cash_coin2')[0].innerHTML;
        $('#popup-charge01').empty();
        $.tmpl(html, dMap).appendTo('#popup-charge01');
      }else if(data.result.code === 999010 || data.result.code === 999011){
        location.href = '/member/login';
      }else{
        alert(`code : ${data.result.code} , msg : ${data.result.msg}`);
      }
    },
    error: function( data ) {
      console.log('error');
      console.log(data);
    }
  });
}

/*
  이더로 코인충전
*/
function ethCoin1(){
  if($("#charge_coin").val() < 0.0001){
    showCommonPopup('0.0001이상을 입력해주세요.', '', 'OK', function (){
    });
    return;
  }

  //일반 지갑 존재 여부 체크
  if(isEmpty(wallet_regular)){
    showCommonPopup('일반 지갑을 먼저 생성해주세요.', '', 'OK', function (){
    });
    return;
  }

  let coin = $("#charge_coin").val();
  let eth = removeComma($("#charge_eth").val());

  if($.trim(coin) === '' || coin <= 0){
    showCommonPopup('수량을 입력해주세요.', '', 'OK', function (){
      $("#charge_coin").focus();
    });
    return;
  }

  if(wallet_regular.eth_balance < eth){
    showCommonPopup('보유 ETH가 부족합니다.', '', 'OK', function (){

    });
    return;
  }

  let dataMap = {
    'coin' : addComma(coin),
    'eth' : addComma(eth)
  };
  let html = $('#charge_eth_coin1')[0].innerHTML;
  $('#popup-charge01').empty();
  $.tmpl(html, dataMap).appendTo('#popup-charge01');

  $('#popup-charge01 .btn_bottom').click(function (){
    ethCoin2(eth, coin);
  });

}

async function ethCoin2(eth, coin){
  await tokenCheck();

  //keystorefile 체크
  if(isEmpty(keyStoreJson_eth)){
    showCommonPopup('KEY STORE FILE을 선택해주세요.', '', 'OK', function (){
    });
    return;
  }

  //레귤러 주소랑 keystorefile랑 같은지 체크
  if(wallet_regular.wallet_address !== ('0x' + JSON.parse(keyStoreJson_eth).address)){
    showCommonPopup('등록된 지갑과 다른 KEY STORE FILE 입니다.', '', 'OK', function (){
    });
    return;
  }

  //비밀번호 입력 체크
  let charge_transaction_pwd = $('#charge_transaction_pwd').val();
  if($.trim(charge_transaction_pwd) === ''){
    showCommonPopup('비밀번호를 입력해주세요.', '', 'OK', function (){
      $('#charge_transaction_pwd').focus();
    });
    return;
  }


  //트랜잭션 호출
  let progressObj = $('#popup-charge01 .loading');
  $('#popup-charge01 #transaction_btn').hide();

  //console.log(eth);
  let tx = await exchangeEtherToToken(charge_transaction_pwd, eth, keyStoreJson_eth, progressObj)
    .catch(function (e){
      //progressObj.hide();
      progressObj.addClass('hidden');
      $('#popup-charge01 #transaction_btn').show();
      showCommonPopup('안내', e.message, 'OK', function (){
      });
    }
  );

  if(!isEmpty(tx)){
    let dataMap = {
      'call_url' : '/api/charge/ethCoin.do',
      'wallet_no' : wallet_regular.wallet_no,
      'purchase_amount' : coin,
      'deposit_price' : eth,
      'transaction_id' : tx.hash
    };

    $.ajax({
      type: "POST",
      url: "/api/postApi",
      data : dataMap,
      dataType: "json",
      success: function( data ) {
        if(data.result.code === 0){
          let dMap = {
            'coin' : coin,
            'eth' : eth,
            'transaction_id' : tx.hash
          };
          let html = $('#charge_eth_coin2')[0].innerHTML;
          $('#popup-charge01').empty();
          $.tmpl(html, dMap).appendTo('#popup-charge01');
        }else if(data.result.code === 999010 || data.result.code === 999011){
          location.href = '/member/login';
        }else{
          alert(`code : ${data.result.code} , msg : ${data.result.msg}`);
        }
      },
      error: function( data ) {
        console.log('error');
        console.log(data);
      }
    });
  }
}


/*
  exchangeEtherToToken
*/
async function exchangeEtherToToken(pwd, value, keyStoreJson, progressObj){
  var wallet = await ethers.Wallet.fromEncryptedJson(keyStoreJson, pwd, function (progress){
    //console.log("Encrypting: " + parseInt(progress * 100) + "% complete");
    progressObj.removeClass('hidden');
    progressObj.children('.loadingbar').css('width', `${parseInt(progress * 100)}%`);

  });

  let gasPrice = await defaultProvider.getGasPrice();
  let walletWithProvider = new ethers.Wallet(wallet.privateKey, defaultProvider);
  let nonce = await walletWithProvider.getTransactionCount();

  let transaction = {
    from: walletWithProvider.address,
    to: erc20_sc_address,
    value: ethers.utils.parseEther(value),
    data: "0x637b55eb",
    nonce: nonce
  }

  // let overrides = {
  //     nonce: nonce,
  //     //gasLimit: ethers.utils.bigNumberify(gasLimit),
  //     gasPrice: gasPrice,
  //     value: ethers.utils.parseEther(value)
  // }
//console.log(transaction);
  await defaultProvider.estimateGas(transaction).then(function(estimate) {
    //console.log(estimate);
    //측정된 gasLimit 에서 10000 을 더한다.
    transaction.gasLimit = estimate.add(ethers.utils.bigNumberify(10000));
    transaction.gasPrice = gasPrice;
  })

  delete transaction.from;  //gasLimit 계산할때는 필요하고 실제 트랜잭션 발생시 from키가 있으면 에러발생을함

  //console.log(transaction.gasLimit);
  let signedTransaction = await wallet.sign(transaction);

  // This can now be sent to the Ethereum network
  let tx = await defaultProvider.sendTransaction(signedTransaction);
  //console.log(tx);


  //var contract = new ethers.Contract(erc20_sc_address, abi, walletWithProvider);
  //var tx = await contract.exchangeEtherToToken(overrides);
  //console.log(tx);

  return tx;
}



/*
  캐시로 이더충전
*/
function cashEth1(){
  getChargeRate();

  let dataMap = {
  };
  let html = $('#charge_cash_eth1')[0].innerHTML;
  $('#popup-charge01').empty();
  $.tmpl(html, dataMap).appendTo('#popup-charge01');

  //1.cash로 eth 충전 계산
  $("#charge_eth").keyup(function() {

    let cash_eth = rate.CASH_ETH;
    let cash = $("#charge_eth").val() / cash_eth;
    $("#charge_cash").val(addComma(Math.round(cash) + ''));

  });
  $('#charge_eth').keypress(function (event) {
    if (event.which && (event.which <= 47 || event.which >= 58) && event.which != 8 && event.which != 46) {
      event.preventDefault();
    }
  });
}

function cashEth2(){
  if($("#charge_eth").val() < 0.0001){
    showCommonPopup('안내', '0.0001이상을 입력해주세요.', 'OK', function (){
    });
    return;
  }

  //일반 지갑 존재 여부 체크
  if(isEmpty(wallet_regular)){
    showCommonPopup('안내', '일반 지갑을 먼저 생성해주세요.', 'OK', function (){
    });
    return;
  }

  let purchase_amount = removeComma($('#charge_eth').val());
  let deposit_price = removeComma($('#charge_cash').val());
  let depositor = $('#depositor').val();

  if($.trim(purchase_amount) === '' || purchase_amount <= 0){
    showCommonPopup('안내', '수량을 입력해주세요.', 'OK', function (){
      $('#charge_eth').focus();
    });
    return;
  }

  if($.trim(depositor) === ''){
    showCommonPopup('안내', '입금자명을 입력해주세요.', 'OK', function (){
      $('#depositor').focus();
    });
    return;
  }

  let dataMap = {
    'call_url' : '/api/charge/cashEth.do',
    'wallet_no' : wallet_regular.wallet_no,
    'purchase_amount' : purchase_amount,
    'deposit_price' : deposit_price,
    'depositor' : depositor
  };

  $.ajax({
    type: "POST",
    url: "/api/postApi",
    data : dataMap,
    dataType: "json",
    success: function( data ) {
      if(data.result.code === 0){
        let dMap = {
          'eth' : $('#charge_eth').val(),
          'cash' : $('#charge_cash').val()
        }
        let html = $('#charge_cash_eth2')[0].innerHTML;
        $('#popup-charge01').empty();
        $.tmpl(html, dMap).appendTo('#popup-charge01');
      }else if(data.result.code === 999010 || data.result.code === 999011){
        location.href = '/member/login';
      }else{
        alert(`code : ${data.result.code} , msg : ${data.result.msg}`);
      }
    },
    error: function( data ) {
      console.log('error');
      console.log(data);
    }
  });
}

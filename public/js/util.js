/*************************************
 ** 오늘 날짜
 *************************************/
function getTimeStamp() {
  var d = new Date();
  var s =
    leadingZeros(d.getFullYear(), 4) +
    leadingZeros(d.getMonth() + 1, 2) +
    leadingZeros(d.getDate(), 2) +

    leadingZeros(d.getHours(), 2) +
    leadingZeros(d.getMinutes(), 2) +
    leadingZeros(d.getSeconds(), 2);

  return s;
}

/*************************************
 **  날짜
 *************************************/
function getFormatDate(date, div) {
  var year = date.getFullYear(); //yyyy
  var month = (1 + date.getMonth()); //M
  month = month >= 10 ? month : '0' + month; // month 두자리로 저장
  var day = date.getDate(); //d

  day = day >= 10 ? day : '0' + day; //day 두자리로 저장

  return (year + div + month + div + day);;
}


/*************************************
 **  날짜 시간
 *************************************/
function getFormatDateTime(date, div, timeDiv) {
  var year = date.getFullYear(); //yyyy
  var month = (1 + date.getMonth()); //M
  month = month >= 10 ? month : '0' + month; // month 두자리로 저장
  var day = date.getDate(); //d

  day = day >= 10 ? day : '0' + day; //day 두자리로 저장

  var h = date.getHours();
  var m = date.getMinutes();
  m = m >= 10 ? m : '0' + m;
  var s = date.getSeconds();
  s = s >= 10 ? s : '0' + s;

  return (year + div + month + div + day + ' ' + h + timeDiv + m + timeDiv + s);;
}

function leadingZeros(n, digits) {
  var zero = '';
  n = n.toString();

  if (n.length < digits) {
    for (i = 0; i < digits - n.length; i++)
      zero += '0';
  }
  return zero + n;
}



function getRandomStr(len) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < len; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}




/*************************************
 ** email 주소형식이 올바른지 검사
 *************************************/
// function checkEmail(strEmail) {
// 	var arrMatch = strEmail.match(/^(\".*\"|[A-Za-z0-9_-]([A-Za-z0-9_-]|[\+\.])*)@(\[\d{1,3}(\.\d{1,3}){3}]|[A-Za-z0-9][A-Za-z0-9_-]*(\.[A-Za-z0-9][A-Za-z0-9_-]*)+)$/);
// 	if (arrMatch == null) {
// 		return false;
// 	}
//
// 	var arrIP = arrMatch[2].match(/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/);
// 	if (arrIP != null) {
// 		for (var i = 1; i <= 4; i++) {
// 			if (arrIP[i] > 255) {
// 				return false;
//       		}
//    		}
// 	}
// 	return true;
// }
function checkEmail(strEmail) {
  var emailRule = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/; //이메일 정규식

  if (!emailRule.test(strEmail)) {
    //경고
    return false;
  }
  return true;

}

/*************************************
 ** password 형식 : 영문,숫자 혼합하여 8-30자리 이내
 *************************************/
function checkPassword(str) {
  var reg_pwd = /^.*(?=.{8,30})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;
  if (!reg_pwd.test(str)) {

    return false;
  }
  return true;
}

/*************************************
 ** 날짜 유효성 체크
 *************************************/
function isValidDate(param) {
  try {
    param = param.replace(/-/g, '');

    // 자리수가 맞지않을때
    if (isNaN(param) || param.length != 8) {
      return false;
    }

    var year = Number(param.substring(0, 4));
    var month = Number(param.substring(4, 6));
    var day = Number(param.substring(6, 8));

    if (month < 1 || month > 12) {
      return false;
    }

    var maxDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var maxDay = maxDaysInMonth[month - 1];

    // 윤년 체크
    if (month == 2 && (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)) {
      maxDay = 29;
    }

    if (day <= 0 || day > maxDay) {
      return false;
    }
    return true;

  } catch (err) {
    return false;
  }
}

/*************************************
 ** 핸드폰번호 유효성 체크
 *************************************/
function isValidPhone(str) {
  var reg_phone = /^\d{11}$/;
  if (str.match(reg_phone)) {
    return true;
  } else {
    return false;
  }
}


/*************************************
 ** 금액 입력시 "," 자동 입력
 *************************************/
// 숫자 타입에서 쓸 수 있도록 format() 함수 추가
Number.prototype.format = function() {
  if (this == 0) return 0;

  var reg = /(^[+-]?\d+)(\d{3})/;
  var n = (this + '');

  while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

  return n;
};

// 문자열 타입에서 쓸 수 있도록 format() 함수 추가
String.prototype.format = function() {
  var num = parseFloat(this);
  if (isNaN(num)) return "0";

  return num.format();
};


function addComma(obj) {
  var regx = new RegExp(/(-?\d+)(\d{3})/);
  var bExists = obj.indexOf(".", 0); //0번째부터 .을 찾는다.
  var strArr = obj.split('.');
  while (regx.test(strArr[0])) { //문자열에 정규식 특수문자가 포함되어 있는지 체크
    //정수 부분에만 콤마 달기
    strArr[0] = strArr[0].replace(regx, "$1,$2"); //콤마추가하기
  }
  if (bExists > -1) {
    //. 소수점 문자열이 발견되지 않을 경우 -1 반환
    obj = strArr[0] + "." + strArr[1];
  } else { //정수만 있을경우 //소수점 문자열 존재하면 양수 반환
    obj = strArr[0];
  }
  return obj; //문자열 반환
  // str = String(str);
  // return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function removeComma(str) {
  str = "" + str.replace(/,/gi, ''); // 콤마 제거
  str = str.replace(/(^\s*)|(\s*$)/g, ""); // trim()공백,문자열 제거
  return str;
  //eturn (new Number(str));//문자열을 숫자로 반환

  // str = String(str);
  // return str.replace(/[^\d]+/g, '');
}

/*************************************
 ** 용량
 *************************************/
function getFileSize(x) {
  var s = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
  var e = Math.floor(Math.log(x) / Math.log(1024));
  return (x / Math.pow(1024, e)).toFixed(2) + " " + s[e];
};

/*************************************
 ** 초를 시분초로 변환
 *************************************/
function getTimeStringSeconds(seconds) {
  var hour, min, sec;
  hour = parseInt(seconds / 3600);
  min = parseInt((seconds % 3600) / 60);
  sec = seconds % 60;

  // if (hour.toString().length==1) hour = '0' + hour;
  // if (min.toString().length==1) min = '0' + min;
  // if (sec.toString().length==1) sec = '0' + sec;

  let str = '';
  if (seconds >= 3600) {
    str += hour + 'h ' + min + 'm ' + sec + 's'
  } else if (seconds < 3600 && seconds >= 60) {
    str += min + 'm ' + sec + 's'
  } else {
    str += sec + 's'
  }

  return str;
}

/*************************************
 ** 넘어온 값이 빈값인지 체크합니다.
 ** !value 하면 생기는 논리적 오류를 제거하기 위해
 ** 명시적으로 value == 사용
 ** [], {} 도 빈값으로 처리
 *************************************/

var isEmpty = function(value) {
  if (value == "" || value == null || value == undefined || (value != null && typeof value == "object" && !Object.keys(value).length)) {
    return true
  } else {
    return false
  }
};



function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
  var byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0)
    byteString = atob(dataURI.split(',')[1]);
  else
    byteString = unescape(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to a typed array
  var ia = new Uint8Array(byteString.length);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ia], {
    type: mimeString
  });
}


/**
 ** input 숫자만 입력
 **/
function numkeyCheck(e, isDecimal) {
  var keyValue = event.keyCode;

  if (((keyValue >= 48) && (keyValue <= 57))) {
    return true;
  } else {
    //소수점체크
    if (isDecimal) {
      if (keyValue === 46) {
        return true;
      }
    }
    return false;
  }
}

function numMinMaxCheck(obj, min, max) {
  if (obj.val() >= min || ob.val() <= max) {
    return true;
  } else {
    return false;
  }
}

function getFuncHexData() {
  var arg = new Array();
  var param = new Array();

  let i = 0;
  for (let val of arguments) {
    arg[i++] = val;
  }

  funcHex = ethers.utils.id(arg[0]).slice(0, 10);

  var data = funcHex;
  for (i = 1; i < arg.length; i++) {
    param[i] = pad(arg[i].slice(2), 64);
    data += param[i];
  }

  return data;
}

function pad(n, width) {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}

/*************************************
 ** 그래프 관련 함수
 *************************************/
function drawBarChart(graph_data, graphSize) {
  Morris.Bar({
    element: 'morris-bar-chart',
    data: graph_data.slice(0, graphSize),
    xkey: 'purchase_date',
    ykeys: ['cnt'],
    labels: ['구매수'],
    hideHover: 'auto',
    resize: true,

  });
}


function showLoading() {
  //화면의 높이와 너비를 구합니다.
    var maskHeight = $(document).height();
    var maskWidth  = window.document.body.clientWidth;

    //화면에 출력할 마스크를 설정해줍니다.
    var mask       = "<div id='mask' style='position:absolute; z-index:9000; background-color:#000000; display:none; left:0; top:0;'></div>";
    var loadingImg = '';

    loadingImg += " <img src='/image/spinner.gif' style='position: absolute; left: 50%; top: 50%; display: block;'/>";

    //화면에 레이어 추가
    $('body')
        .append(mask)

    //마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채웁니다.
    $('#mask').css({
            'width' : maskWidth,
            'height': maskHeight,
            'opacity' : '0.3'
    });

    //마스크 표시
    $('#mask').show();

    //로딩중 이미지 표시
    $('#mask').append(loadingImg);

}

function hideLoading() {
  $('#mask').hide();
  $('#mask').empty();

}

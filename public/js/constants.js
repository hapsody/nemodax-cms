//구매 상태
const PURCHASE_STAT = {
  "00": {msg: "구매완료"},
  "01": {msg: "구매진행중"},
  "98": {msg: "환불접수"},
  "97": {msg: "환불검토완료"},
  "96": {msg: "신고삭제"}
}
Object.freeze(PURCHASE_STAT);

//환불결과
const REFUND_RESULT = {
  "00": {msg: "대기"},
  "01": {msg: "환불 처리 완료"},
  "02": {msg: "환불 대상 아님"}
}
Object.freeze(REFUND_RESULT);

//신고타입
const FILE_REPORT_TYPE = {
  "00": {msg: "허위 데이터 신고"},
  "01": {msg: "불법 데이터 신고"},
  "02": {msg: "불법 성인 데이터 신고"},
  "03": {msg: "불법 복제 데이터 신고"},
  "99": {msg: "기타"}
}
Object.freeze(FILE_REPORT_TYPE);

//신고결과
const FILE_REPORT_RESULT = {
  "00": {msg: "검토중"},
  "01": {msg: "불법 데이터 사실확인"},
  "02": {msg: "신고 사실 불일치"}
}
Object.freeze(FILE_REPORT_RESULT);


//신고보상
const FILE_REPORT_REWARD = {
  "00": {msg: "지급대기"},
  "01": {msg: "지급완료"},
  "02": {msg: "지급대상아님"}
}
Object.freeze(FILE_REPORT_REWARD);

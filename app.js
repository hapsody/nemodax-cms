var createError = require('http-errors');
var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cookie = require('cookie');
var helmet = require('helmet');

var indexRouter = require('./routes/index');
var adminRouter = require('./routes/api/admin');
var memberRouter = require('./routes/api/member');
var topInfoRouter = require('./routes/api/topInfo');
var noticeRouter = require('./routes/api/notice');
var cplogoRouter = require('./routes/api/cpLogo');
var contentRouter = require('./routes/api/content');
var chargeRouter = require('./routes/api/charge');
var purchaseRouter = require('./routes/api/purchase');

var app = express();


// use helmet
app.use(helmet());
app.disable('x-powered-by');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.set('layout', 'layouts/layout');
app.use(expressLayouts);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



// 로그인이 필요한 부분 체크하는 미들웨어
// TODO. 추후 다른 파일로 분리.
app.use(function (req, res, next){
  var not_login_path_list = [
    '/login',
    '/api/admin/login',

  ];

  //cookie 로 로그인 여부 체크하여 response 에 설정함.
  let is_login = false;
  if(req.headers.cookie !== undefined && cookie.parse(req.headers.cookie).NMCT !== undefined){
    is_login = true;
    res.locals.mem_no = cookie.parse(req.headers.cookie).n_user;

  }
  req.is_login = is_login;
  res.locals.is_login = is_login;



  //로그인 여부에 따라 처리
  if(not_login_path_list.indexOf(req.path) > -1){
    console.log('login check pass');
    next();
  }else{
    if(is_login){
      next();
    }else{
      if(req.xhr){
        var result = {'code' : 999010};
        var d = {'result': result};

        res.json(d);
      }else{
        res.redirect('/login');

      }

    }
  }
});


app.use(function (req, res, next){
  let config;

  if(!process.env.NODE_ENV){
    config = require('./config/local');
  }else if(process.env.NODE_ENV == 'dev'){
    config = require('./config/dev');
  }else if(process.env.NODE_ENV == 'real'){
    config = require('./config/real');
  }

  req.config = config;
  res.locals.config = config;
  next();
});


app.use('/', indexRouter);
app.use('/api/admin', adminRouter);
app.use('/api/member', memberRouter);
app.use('/api/topInfo', topInfoRouter);
app.use('/api/notice', noticeRouter);
app.use('/api/cplogo', cplogoRouter);
app.use('/api/content', contentRouter);
app.use('/api/charge', chargeRouter);
app.use('/api/purchase', purchaseRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.locals.title = 'Page Not Found';
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log('err :: ', err);

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('error');
  res.json('error');
});

module.exports = app;

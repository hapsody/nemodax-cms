const local = module.exports;

local.info = {
  'id' : 'local',
  'api_server_host' : 'devapi.nemodax.com',
  'api_server_port' : '443',
  'erc20_sc_address' : '0x6450de6fb05367a59e9cead5abe66c9bcb80a20a',
  'point_charge_address' : '0x6450de6fb05367a59e9cead5abe66c9bcb80a20a',
  'etherscan_web_uri' : 'https://rinkeby.etherscan.io',
  's3_bucket' : 'nemodax-upload-dev',
  'file_server_url' : 'https://s3.ap-northeast-2.amazonaws.com/nemodax-dev',
  'data':{
    "address": "ea55cb6bff4b256f6ee532691dbdb12fc4386c7d",
    "crypto": {
      "cipher": "aes-128-ctr",
      "ciphertext": "41961970a398a4433c411b925b579b9acdbe5fa4df13b99e6f8865bb8b25f0ff",
      "cipherparams": {
        "iv": "9b5672b182c6a2b4cda07bbab48b60bb"
      },
      "kdf": "scrypt",
      "kdfparams": {
        "dklen": 32,
        "n": 262144,
        "p": 1,
        "r": 8,
        "salt": "a3bed049f4c70ca91ffb70b739179d3a73c4d49e1fc4e6fbb111d210e9247c23"
      },
      "mac": "c523220c5a060334dafe7fedb781f6f50390a6e6d972acdf7340a3321a354605"
    },
    "id": "933534cd-a4f0-496e-a259-130b61e0c0f5",
    "version": 3
  },
  'blockchain_network':'rinkeby',
  'sqs_url': 'https://sqs.ap-northeast-2.amazonaws.com/591672227761/nemodax-imagemove-dev'
}

const real = module.exports;

real.info = {
  'id' : 'real',
  'api_server_host' : 'api.nemodax.com',
  'api_server_port' : '443',
  'erc20_sc_address' : '0x957b28f93B0e01557E21e6c564Ab26ddc2d18EC5',
  'point_charge_address' : '0x957b28f93B0e01557E21e6c564Ab26ddc2d18EC5',
  'etherscan_web_uri' : 'https://etherscan.io',
  's3_bucket' : 'nemodax-upload',
  'file_server_url' : 'https://s3.ap-northeast-2.amazonaws.com/nemodax',
  'data':{
    "address": "e57aaf2a5e31cfc894625be3a12d314762298e2c",
    "crypto": {
      "cipher": "aes-128-ctr",
      "ciphertext": "f71b950692770f49c383056a5e8b1066a76440e80444992b1d864f72a10d3540",
      "cipherparams": {
        "iv": "075dd07775d2eb858a69a69df2561868"
      },
      "kdf": "scrypt",
      "kdfparams": {
        "dklen": 32,
        "n": 262144,
        "p": 1,
        "r": 8,
        "salt": "83802689b323857791edd159af43df701299963a7b9e1e67967946205a8cb162"
      },
      "mac": "0d17e2cf1948a577ce012503044fd044850c1bd4d6f36cff6e380ec7f941d4d2"
    },
    "id": "b799adae-b993-4dfb-b8b1-575eaec1a2f4",
    "version": 3
  },
  'blockchain_network':'homestead',
  'sqs_url': 'https://sqs.ap-northeast-2.amazonaws.com/591672227761/nemodax-imagemove'
}

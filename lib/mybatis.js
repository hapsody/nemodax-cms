var path = require('path');
const mysql2  = require('mysql2');
const mybatisMapper = require('mybatis-mapper');


var env = process.env.NODE_ENV || 'local';
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

// create the connection to database
const connection = mysql2.createConnection({
  host: config.host,
  user: config.username,
  database : config.database,
  password : config.password
});



// create the myBatisMapper from xml file
mybatisMapper.createMapper([ './sql/member.xml',
                              './sql/topNotice.xml',
                              './sql/notice.xml',
                              './sql/cpLogo.xml',
                              './sql/content.xml',
                              './sql/charge.xml',
                              './sql/purchase.xml'  
                            ]);


module.exports = {mybatisMapper, connection};

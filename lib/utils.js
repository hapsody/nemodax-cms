const ethers = require('ethers');

//./lib/utils.js
queryResultParse = function(result){
	return JSON.parse(JSON.stringify(result));
};



/*************************************
 ** 그래프 관련 함수들
 *************************************/

 splitFormattedDayToElements = function (formattedDay) { //YYYYMMDD형식의 스트링을 연,월,일로 쪼개어 객체로 저장하는 함수
  var obj = {
    year: "",
    month: "",
    date: ""
  }
  obj.year = formattedDay.slice(0, 4);
  obj.month = formattedDay.slice(4, 6);
  obj.date = formattedDay.slice(6, 8);
  return obj;
}

getFormattedDay = function (target) { // Date 객체 형식을 => YYYYMMDD 20190408로 바꿔주는 함수
  var targetYear = target.getFullYear().toString();
  var targetMonth = (target.getMonth() + 1).toString(); // getMonth한 달은 0월부터 시작이라 +1을 해줌
  targetMonth = (targetMonth.length == 1) ? "0" + targetMonth : targetMonth; // 한자릿수 달은 앞에 0을 삽입해
  var targetDate = target.getDate().toString();
  targetDate = (targetDate.length == 1) ? "0" + targetDate : targetDate; // 한자릿수 달은 앞에 0을 삽입해

  return targetYear + targetMonth + targetDate;
}

goYesterday = function (targetYear, targetMonth, targetDate) { //입력받은 날짜의 직전일을 구해서 YYYYMMDD형식으로 리턴하는 함수
  var target = new Date();
  var yesterDay = new Date(target.setFullYear(targetYear, targetMonth - 1, targetDate) - (24 * 60 * 60 * 1000)); //밀리세컨드 단위을 넣어서 전일 date 객체를 얻

  return getFormattedDay(yesterDay);
}

addZeroCntToGraph = function(data) {
  var target = data.startDay;

  for (data.i = data.graph_size - 1; data.i >= 0; data.i--) {
    if (data.endOfData) {
      data.graph_data[data.i] = {
        purchase_date: targetObj.year.toString() + targetObj.month.toString() + targetObj.date.toString(),
        cnt: "0"
      }

      target = goYesterday(targetObj.year, targetObj.month, targetObj.date);
      continue;
    }

    var targetObj = splitFormattedDayToElements(target);

    var record = data.db_data[data.j].purchase_date;
    var recordObj = splitFormattedDayToElements(record);


    if (targetObj.year == recordObj.year &&
      targetObj.month == recordObj.month &&
      targetObj.date == recordObj.date) {
      data.graph_data[data.i] = data.db_data[data.j];

      if (data.j + 1 < data.db_length) {
        data.j++;
        target = goYesterday(targetObj.year, targetObj.month, targetObj.date);
      } else {
        data.endOfData = true;
      }
    } else {
      data.graph_data[data.i] = {
        purchase_date: targetObj.year.toString() + targetObj.month.toString() + targetObj.date.toString(),
        cnt: "0"
      }

      target = goYesterday(targetObj.year, targetObj.month, targetObj.date);
    }
  }
  return data.graph_data;
}

/*************************************
 ** 트랜잭션 관련 함수들
 *************************************/
function pad(n, width) {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}



function getFuncHexData() {
  var arg = new Array();
  var param = new Array();

  let i = 0;
  for (let val of arguments) {
    arg[i++] = val;
  }

  funcHex = ethers.utils.id(arg[0]).slice(0, 10);

  var data = funcHex;
  for (i = 1; i < arg.length; i++) {
    param[i] = pad(arg[i].slice(2), 64);
    data += param[i];
  }

  return data;
}


module.exports = {
	queryResultParse: queryResultParse,
	splitFormattedDayToElements: splitFormattedDayToElements,
	getFormattedDay: getFormattedDay,
	goYesterday: goYesterday,
	addZeroCntToGraph: addZeroCntToGraph,
	pad: pad,
	getFuncHexData:getFuncHexData
}
